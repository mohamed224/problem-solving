package sort;

public class BubbleSort {
	public static void main(String[] args) {
		int[] values = { 5, 1, 6, 2, 4, 3 };
		int res[] = bubbleSort(values);
		for (int i : res) {
			System.out.println(i);
		}
	}

	public static int[] bubbleSort(int[] values) {
		int temp = 0, n = values.length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - i - 1; j++) {

				if (values[j] > values[j + 1]) {
					temp = values[j];
					values[j] = values[j + 1];
					values[j + 1] = temp;
				}
			}
		}
		return values;
	}

}
