package aprilChallenge;

public class MaximumSubarray {

	public static void main(String[] args) {
		int[] nums = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		System.out.println(maxSubArray(nums));
	}

	public static int maxSubArray(int[] nums) {
		int res = Integer.MIN_VALUE;
		int value = 0;
		for (int x : nums) {
			value += x;
			res = Math.max(value, res);
			value = Math.max(value, 0);
		}
		return res;
	}
}
