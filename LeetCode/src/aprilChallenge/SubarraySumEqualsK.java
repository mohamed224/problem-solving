package aprilChallenge;

public class SubarraySumEqualsK {
	
	public static void main(String[] args) {
		int [] nums = {1,1,1};
		System.out.println(subarraySum(nums, 2));
	}
	
	 public static int subarraySum(int[] nums, int k) {
	       int count=0 ,n=nums.length;
	       for(int i=0; i<n;i++){
	           int sum=0;
	           for(int j=i; j<n;j++){
	               sum+=nums[j];
	               if(k==sum){
	                   count++;
	               }
	           }
	       }
	        return count;
	    }

}
