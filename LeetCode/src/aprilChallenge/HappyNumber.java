package aprilChallenge;

import java.util.HashSet;
import java.util.Set;

public class HappyNumber {

	public static void main(String[] args) {
			System.out.println(isHappy(7));
	}

	public static boolean isHappy(int n) {
		Set<Integer> visited = new HashSet<>();
		while (true) {
			if (n == 1) {
				return true;
			}
			n = digitSquareSum(n);
			if (visited.contains(n)) {
				return false;
			} else {
				visited.add(n);
			}
		}

	}

	public static int digitSquareSum(int n) {
		int sum = 0;
		while (n != 0) {
			int digits = n % 10;
			n /= 10;
			sum += digits * digits;
		}
		return sum;
	}

}
