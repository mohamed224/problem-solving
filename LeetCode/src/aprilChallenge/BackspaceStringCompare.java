package aprilChallenge;

import java.util.Stack;

public class BackspaceStringCompare {

	public static void main(String[] args) {
		System.out.println(backspaceCompare("ab#c", "ad#c"));
	}
	
	public static boolean backspaceCompare(String S, String T) {
		
		return build(S).equals(build(T));
	}
	
	public static Stack<Character> build(String val) {
		Stack<Character> stk = new Stack<>();
		for(int i=0;i<val.length();i++) {
			if(!stk.isEmpty() && val.charAt(i)=='#') {
				stk.pop();
			}else if(val.charAt(i)!='#') {
				stk.push(val.charAt(i));
			}
		}
		return stk;
	}
}
