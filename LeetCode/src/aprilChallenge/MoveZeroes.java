package aprilChallenge;

public class MoveZeroes {

	public static void main(String[] args) {
		
	}
	
	public static void moveZeroes(int[] nums) {
		int n = nums.length , j=0;
		for(int i:nums) {
			if(nums[i]!=0) {
				nums[j++]=nums[i];
			}
		}
		for(int k=j;k<n;k++) {
			nums[k]=0;
		}
	}
}
