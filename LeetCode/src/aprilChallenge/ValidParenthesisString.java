package aprilChallenge;

import java.util.Deque;
import java.util.LinkedList;

public class ValidParenthesisString {

	public static void main(String[] args) {
		System.out.println(checkValidString("(*)))"));
	}

	public static boolean checkValidString(String s) {
		int balance=0, n=s.length();
		
		if(n==0) {
			return true;
		}
		for(int i=0;i<n;i++) {
			if(s.charAt(i)==')') {
				balance--;
			}else {
				balance++;
			}
			if(balance<0) {
				return false;
			}
		}
		balance=0;
		for(int i=n;i>=0;i--) {
			if(s.charAt(i)=='(') {
				balance--;
			}else {
				balance++;
			}
			if(balance<0) {
				return false;
			}
		}
		
		
		return true;
	}
	
	 public boolean checkValidString2(String s) {
	        
	        Deque<Integer> paraStack = new LinkedList<>();
	        Deque<Integer> starStack = new LinkedList<>();
	        
	        for(int i = 0 ; i< s.length(); i++){
	            char ch = s.charAt(i);
	            
	            if(ch == '(')
	                paraStack.push(i);
	            else if(ch == '*')
	                starStack.push(i);
	            else{
	                if(!paraStack.isEmpty())
	                    paraStack.pop();
	                else if(!starStack.isEmpty())
	                    starStack.pop();
	                else
	                    return false;
	            }        
	        }
	        
	        return isBalancedStack(paraStack,starStack);
	    }
	    private boolean isBalancedStack(Deque<Integer> paraStack, Deque<Integer> starStack){
	        
	        while(!paraStack.isEmpty()&& !starStack.isEmpty()){
	            if(paraStack.pop() > starStack.pop())
	                return false;
	        }
	        return paraStack.isEmpty();
	        
	    }

}
