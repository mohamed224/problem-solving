package aprilChallenge;

import java.util.Arrays;

public class LastStoneWeight {
	public static void main(String[] args) {
		int [] stones = {2,7,4,1,8,1};
		System.out.println(lastStoneWeight(stones));
	}

	public static int lastStoneWeight(int[] stones) {
		int n = stones.length;

		if (n == 1) {
			return stones[0];
		}
		for (int i = 0; i < n; i++) {
			Arrays.sort(stones);
			if (stones[n - 1] == stones[n - 2]) {
				stones[n - 1] = 0;
				stones[n - 2] = 0;
			} else {
				stones[n - 1] = stones[n - 1] - stones[n - 2];
				stones[n - 2] = 0;
			}
		}
		return stones[n - 1] == 0 && stones[n - 2] == 0 ? 0 : stones[n - 1];
	}

}
