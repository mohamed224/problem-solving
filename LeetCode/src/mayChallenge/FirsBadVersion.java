package mayChallenge;


/* The isBadVersion API is defined in the parent class VersionControl.
boolean isBadVersion(int version); */

public class FirsBadVersion {
	public static void main(String[] args) {
		System.out.println(firstBadVersion(5));
	}

	public static int firstBadVersion(int n) {
		int d = 1, f = n, res = -1;
		while (d <= f) {
			int m = (d + (f - d) / 2);
			if (isBadVersion(m)) {
				res = m;
				f = m - 1;
			} else {
				d = m + 1;
			}
		}
		return res;
	}
}
