package mayChallenge;

public class FirstUniqueCharacterInAString {

	public static void main(String[] args) {
		System.out.println(firstUniqChar("leetcode"));
	}
	
	  public static int firstUniqChar(String s) {
	        char [] count = new char[26];
	        for(int i=0;i<s.length();i++){
	            char c=s.charAt(i);
	            count[c-'a']++;
	        }
	        for(int i=0;i<s.length();i++){
	            char c=s.charAt(i);
	            if(count[c-'a']==1){
	                return i;
	            }
	        }
	        return -1;
	        
	    }
}
