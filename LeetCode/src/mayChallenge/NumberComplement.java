package mayChallenge;

public class NumberComplement {

	public static void main(String[] args) {
		System.out.println(findComplement(1));
	}

	public static int findComplement(int num) {
		int bitLength = (int) (Math.log(num) / Math.log(2)) + 1;
		int bitMask = (1 << bitLength) - 1;
		return num ^ bitMask;
	}
}
