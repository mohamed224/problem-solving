package mayChallenge;

import java.util.HashMap;
import java.util.Map;

public class SortCharactersByFrequency {
	public static void main(String[] args) {

		System.out.println(frequencySort(""));
	}

	public static String frequencySort(String s) {
		StringBuilder ans = new StringBuilder();
		Map<Character, Integer> count = new HashMap<>();
		for (int i = 0; i < s.length(); i++) {
			count.put(s.charAt(i), count.getOrDefault(s.charAt(i), 0) + 1);
		}
		for(Character c:count.keySet()) {
			System.out.println(c);
			System.out.println(count.get(c));
		}
		while (ans.length() < s.length()) {
			int max = -1; char currChar=' ';
			for(Character c:count.keySet()) {
				if(count.get(c)>max) {
					max=count.get(c);
					currChar=c;
				}
			}
			count.put(currChar,0);
			for(int i=0;i<max;i++) {
				ans.append(currChar);
			}
		}
		return ans.toString();
	}

}
