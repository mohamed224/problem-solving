package mayChallenge;

import java.util.HashSet;
import java.util.Set;

public class JewelsAndStones {

	public static void main(String[] args) {
		String J="aAA", S="aAAbb";
		System.out.println(numJewelsInStones(J, S));

	}

	public static int numJewelsInStones(String J, String S) {
		Set<Character> set = new HashSet<>();
		for(int i=0;i<J.length();i++) {
			set.add(J.charAt(i));
		}
		int result=0;
		for(int i=0;i<S.length();i++) {
			if(set.contains(S.charAt(i))) {
				result++;
			}
		}
		return result;
	}

}
