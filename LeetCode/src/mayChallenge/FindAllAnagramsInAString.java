package mayChallenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindAllAnagramsInAString {

	public static void main(String[] args) {

		System.out.println(findAnagrams("abab", "ab"));
	}

	public static  List<Integer> findAnagrams(String s, String p) {
		List<Integer> res = new ArrayList<>();
		char [] pChar = p.toCharArray();
		for(int i=0;i<s.length();i++) {
			try {
				String val = s.substring(i+1,p.length()+1);
				char [] valChar = val.toCharArray();
				Arrays.sort(pChar);
				Arrays.sort(valChar);
				if(Arrays.equals(valChar,pChar)) {
					res.add(i);
				}
			} catch (Exception e) {
				return res;
			}
			
		}
		return res;
	}
}
