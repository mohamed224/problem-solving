package mayChallenge;

public class FindTheTownJudge {

	public static void main(String[] args) {
		 int N = 4;
		 int [][] trust = {{1,3},{1,4},{2,3},{2,4},{4,3}};
		System.out.println(findJudge(N, trust));
	}

	public static int findJudge(int N, int[][] trust) {
		int [] indegree = new int[N+1];
		int [] outdegree = new int[N+1];
		
		for(int i=0;i<trust.length;i++) {
			outdegree[trust[i][0]]++;
			indegree[trust[i][1]]++;
		}
		
		for(int i =1;i<=N;i++) {
			if(indegree[i]==N-1 && outdegree[i]==0) {
				return i;
			}
		}
		return -1;
	}
}
