package mayChallenge;

public class RansomNote {

	public static void main(String[] args) {
		System.out.println(canConstruct("aa", "aab"));

	}
	
	 public static boolean canConstruct(String ransomNote, String magazine) {
		if(ransomNote.length()>magazine.length()) {
			return false;
		}
		int [] cnt = new int[26];
		for(char c:magazine.toCharArray()) {
			cnt[c-'a']++;
		}
		for(char c:ransomNote.toCharArray()) {
			if(cnt[c-'a']>0) {
				cnt[c-'a']--;
			}else {
				return false;
			}
		}
		return true;
	    }

}
