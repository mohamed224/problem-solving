package mayChallenge;

import java.util.Arrays;

public class PermutationInString {

	public static void main(String[] args) {
		System.out.println(checkInclusion("abc", "bbbca"));
	}
	
	public static boolean checkInclusion(String s1, String s2) {
		int n=s1.length(), l=s2.length(),count=0;
		char [] arr = s2.toCharArray();
		if(n>l) {
			return false;
		}
		for(int i:s1.toCharArray()) {
			count+=i;
		}
		for(int i=0;i<l;i++) {
			int val=0;
			if(l-i<n) {
				return false;
			}
			for(int j=i;j<i+n;j++) {
				val+=arr[j];
			}
			if(val==count) {
				String sub = s2.substring(i,i+n);
				char [] subArr = sub.toCharArray();
				Arrays.sort(subArr);
				char [] sArr= s1.toCharArray();
				Arrays.sort(sArr);
				if(Arrays.equals(subArr, sArr)) {
					return true;
				}
				
			}
			
		}
        return false;
    }
}
