package mayChallenge;

import java.util.Stack;

public class RemoveKDigits {
	
	public static void main(String[] args) {
		String num = "1432219";
		System.out.println(removeKdigits(num, 3));
	}
	
	public static String removeKdigits(String num, int k) {
		if(k==num.length()) {
			return "0";
		}
		if(k==0) {
			return num;
		}
		Stack<Character> stkChar = new Stack<>();
		
		for(char c:num.toCharArray()) {
			while(!stkChar.isEmpty() && k>0 && stkChar.peek()>c) {
				stkChar.pop();
				k--;
			}
			stkChar.push(c);
		}
		for(int i=0;i<k;i++) {
			stkChar.pop();
		}
		StringBuilder sbChar = new StringBuilder();
		while(!stkChar.isEmpty()) {
			sbChar.append(stkChar.pop());
		}
		
		sbChar.reverse();
		while(sbChar.length()>1 && sbChar.charAt(0)=='0') {
			sbChar.deleteCharAt(0);
		}
		return sbChar.toString();
	}

}
