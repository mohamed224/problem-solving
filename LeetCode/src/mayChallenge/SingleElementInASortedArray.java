package mayChallenge;

public class SingleElementInASortedArray {

	public static void main(String[] args) {

		int [] nums = {1,1,2,3,3,4,4,8,8};
		System.out.println(singleNonDuplicate(nums));
	}

	public static int singleNonDuplicate(int[] nums) {

		int si = 0, ei = nums.length - 1;
		while (si < ei) {
			int mid = (si + (ei - si) / 2);
			if (nums[mid - 1] == nums[mid]) {
				boolean isEven = ((ei - mid) % 2 == 0);
				if (isEven) {
					ei = mid - 2;
				} else {
					si = mid + 1;
				}
			} else if (nums[mid] == nums[mid + 1]) {
				boolean isEven = ((ei - mid + 1) % 2 == 0);
				if (isEven) {
					ei = mid - 1;
				} else {
					si = mid + 2;
				}
			} else {
				return nums[mid];
			}
		}
		return nums[si];
	}

}
