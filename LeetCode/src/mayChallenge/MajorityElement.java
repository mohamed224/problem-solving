package mayChallenge;

import java.util.HashMap;
import java.util.Map;

public class MajorityElement {
	public static void main(String[] args) {
		int [] nums =  {1,1,1,2,2,2,3,1};
		System.out.println(majorityElement(nums));
	}
	
	 public static int majorityElement(int[] nums) {
		 Map<Integer, Integer> records = new HashMap<>();
		 int n = nums.length;
		 for(int i:nums) {
			 records.put(i, records.getOrDefault(i, 0)+1);
		 }
		 for(int i:records.keySet()) {
			 if(records.get(i)>(n/2)) {
				 return i;
			 }
		 }
	        return 0;
	    }
}
