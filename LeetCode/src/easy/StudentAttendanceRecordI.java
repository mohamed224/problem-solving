package easy;

public class StudentAttendanceRecordI {
	
	public static void main(String[] args) {
		
	}
	
	
	public static boolean checkRecord(String s) {
		int late=0,absent=0;
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)=='L'){
                late++;
            }else if(s.charAt(i)=='A'){
                absent++;
                late=0;
            }else{
                late=0;
            }
            if(absent>1 || late>2){
                return false;
            }
        }
        
		return true;
	}

}
