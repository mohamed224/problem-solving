package easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ValidParentheses {

	public static void main(String[] args) {

		System.out.println(isValid("()[]{}"));
	}

	public static boolean isValid(String s) {
		Map<Character, Character> map = new HashMap<>();
		Stack<Character> stk = new Stack<>();
		map.put(')','(');
		map.put(']','[');
		map.put('}','{');
		for(char c:s.toCharArray()) {
			if(map.containsKey(c)) {
				if(stk.isEmpty() || map.get(c)!=stk.pop()) {
					return false;
				}
			}
			else {
				stk.add(c);
			}
		}
		
		return stk.isEmpty();
	}
}
