package easy;

public class LongestCommonPrefix {
	
	public static void main(String[] args) {
		String [] strs = {"flower","flow","flight"};
		System.out.println(longestCommonPrefix(strs));
	}
	
	 public static String longestCommonPrefix(String[] strs) {
		 if(strs.length==0) {
			 return "";
		 }
		 
		 StringBuilder reference = new StringBuilder(strs[0]);
		 for(int i=0;i<strs.length;i++) {
			 while(!strs[i].startsWith(reference.toString())) {
				 reference=reference.deleteCharAt(reference.length()-1);
			 }
		 }
	        return reference.toString();
	    }

}
