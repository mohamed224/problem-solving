package easy;

public class PalindromeNumber {

	public static void main(String[] args) {

		System.out.println(isPalindrome(-121));
	}

	public static boolean isPalindrome(int x) {
		int reversedNumber = 0, tmp = x;
		if (x < 0) {
			return false;
		}
		while (tmp != 0) {
			reversedNumber = reversedNumber * 10 + tmp % 10;
			tmp = tmp / 10;
		}
		return x == reversedNumber;
	}

}
