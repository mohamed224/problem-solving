package search;

public class JumpSearch {

	public static void main(String[] args) {
		int [] values = {-5,-3,0,1,2,4};
		System.out.println(jumpSearch(values, 0));
	}
	
	public static int jumpSearch(int values[], int target) {
		   int i = 0;
		   int n = values.length;
		   int m = (int) Math.sqrt(n); 

		   while(values[m] <= target && m < n) { 
		      
		      i = m;  
		      m += Math.sqrt(n);
		      if(m > n - 1)  
		         return -1; 
		   }

		   for(int x = i; x<m; x++) { 
		      if(values[x] == target)
		         return x; 
		   }
		   return -1;
		}


}
