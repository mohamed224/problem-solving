package search;

public class BinarySearch {
	public static void main(String[] args) {

		int [] values = {-5,-3,0,1,2,4};
		System.out.println(binarySearch(values, -5));
	}

	public static int binarySearch(int[] values, int target) {

		int n = values.length;
		int min = 0;
		int max = n - 1;
		int middle = 0;
		while (max >= min) {
			middle = (max + min) / 2;
			if (values[middle] == target) {
				return middle;
			} else if (middle > target) {
				max = middle - 1;
			} else {
				min = middle + 1;
			}
		}
		return -1;

	}

}
