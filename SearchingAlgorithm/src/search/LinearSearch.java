package search;

public class LinearSearch {

	public static void main(String[] args) {

		int [] values = {2,4,-5,0,1,-3};
		
		System.out.println(linearSearch(values, -5));
	}
	
	/*
	 * - values[] => array with all the values 
	 * - target => value to be found
	 * - method will return -1 if target do not exist or will return index of target 
	 */

	public static int linearSearch(int values[], int target) {
		int n = values.length;
		for (int i = 0; i < n; i++) {
			if (values[i] == target) {
				return i;
			}
		}
		return -1;
	}

}
